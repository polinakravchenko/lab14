﻿
// lab14_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <array>
#include <chrono> 
#include <cstddef> 
#include <iostream>
#include <numeric> 

const int g_arrayElements{ 300 };

class Timer
{
private:
    using clock_type = std::chrono::steady_clock;
    using second_type = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_type> m_beg{ clock_type::now() };

public:

    void reset()
    {
        m_beg = clock_type::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_type>(clock_type::now() - m_beg).count();
    }
};

void sortArray(std::array<int, g_arrayElements>& array)
{

    for (std::size_t startIndex{ 0 }; startIndex < (g_arrayElements - 1); ++startIndex)
    {
     
        std::size_t smallestIndex{ startIndex };


        for (std::size_t currentIndex{ startIndex + 1 }; currentIndex < g_arrayElements; ++currentIndex)
        {
       
            if (array[currentIndex] < array[smallestIndex])
            {
          
                smallestIndex = currentIndex;
            }
        }

    
        std::swap(array[startIndex], array[smallestIndex]);
    }
}

int main()
{
    std::array<int, g_arrayElements> array;
    std::iota(array.rbegin(), array.rend(), 1); 

    Timer t;

    sortArray(array);

    std::cout << "Time taken: " << t.elapsed() << " seconds\n";

    return 0;
}